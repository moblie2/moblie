import 'package:flutter/material.dart';

enum APP_THEME{LIGHT, DARK}

void main() {
  runApp(Weather());
}

class MyAppTheme {
  static ThemeData appThemeLight() {
    return ThemeData(
      brightness: Brightness.light,
      appBarTheme: AppBarTheme(
        color: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
      ),
      iconTheme: IconThemeData(
        color: Colors.pink,
      ),
      floatingActionButtonTheme: FloatingActionButtonThemeData(
        backgroundColor: Colors.black,
        foregroundColor: Colors.white,
      ),
      listTileTheme: ListTileThemeData(
        iconColor: Colors.pink,
      ),
    );
  }
  static ThemeData appThemeDark() {
    return ThemeData(
      brightness: Brightness.dark,
      appBarTheme: AppBarTheme(
        color: Colors.black12,
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
      ),
      iconTheme: IconThemeData(
        color: Colors.white,
      ),
      floatingActionButtonTheme: FloatingActionButtonThemeData(
        backgroundColor: Colors.white,
        foregroundColor: Colors.black,
      ),
      listTileTheme: ListTileThemeData(
        iconColor: Colors.white,
      ),
    );
  }
}

class Weather extends StatefulWidget{
  @override
  State<Weather> createState() => _Weather();
}

class _Weather extends State<Weather> {
  var currentTheme = APP_THEME.LIGHT;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currentTheme == APP_THEME.DARK
          ? MyAppTheme.appThemeLight()
          : MyAppTheme.appThemeDark(),
      home: Scaffold(
        appBar: buildAppBarWidget(),
        body: buildBodyWidget(),
      ),
    );
  }
}

Widget buildWeather1() {
  return Column(
    children: <Widget>[
      Text("12:00"),
      Text(""),
      Icon(
        Icons.sunny,
        // color: Colors.indigo.shade800,
      ),
      Text(""),
      Text("30°"),
    ],
  );
}
Widget buildWeather2() {
  return Column(
    children: <Widget>[
      Text("13:00"),
      Text(""),
      Icon(
        Icons.thunderstorm,
        // color: Colors.indigo.shade800,
      ),
      Text(""),
      Text("30°"),
    ],
  );
}
Widget buildWeather3() {
  return Column(
    children: <Widget>[
      Text("14:00"),
      Text(""),
      Icon(
        Icons.thunderstorm,
        // color: Colors.indigo.shade800,
      ),
      Text(""),
      Text("30°"),
    ],
  );
}
Widget buildWeather4() {
  return Column(
    children: <Widget>[
      Text("15:00"),
      Text(""),
      Icon(
        Icons.thunderstorm,
        // color: Colors.indigo.shade800,
      ),
      Text(""),
      Text("30°"),
    ],
  );
}
Widget buildWeather5() {
  return Column(
    children: <Widget>[
      Text("16:00"),
      Text(""),
      Icon(
        Icons.thunderstorm,
        // color: Colors.indigo.shade800,
      ),
      Text(""),
      Text("30°"),
    ],
  );
}
Widget buildWeather6() {
  return Column(
    children: <Widget>[
      Text("17:00"),
      Text(""),
      Icon(
        Icons.thunderstorm,
        // color: Colors.indigo.shade800,
      ),
      Text(""),
      Text("30°"),
    ],
  );
}

//ListTile
Widget TodayListTile() {
  return ListTile(
    title: Text("Today"),
    subtitle: Text("27° / 36°"),
    trailing: Icon(Icons.thunderstorm),
  );
}
Widget TomorrowListTile() {
  return ListTile(
    title: Text("Tomorrow"),
    subtitle: Text("27° / 35°"),
    trailing: Icon(Icons.sunny),
  );
}

AppBar buildAppBarWidget() {
  return AppBar(
    // backgroundColor: Colors.white,
    leading: IconButton(
      icon: Icon(Icons.menu),
      // color: Colors.black,
      onPressed: (){
        print("Menu");
      },
    ),
    actions: <Widget>[
      IconButton(
        icon: Icon(Icons.settings),
        // color: Colors.black,
        onPressed: (){
          print("Settings");
        },
      ),
    ],
  );
}
Widget buildBodyWidget() {
  return ListView (
      children: <Widget>[
        Column(
          children: <Widget>[
            Container(
              width: double.infinity,
              //Height constraint at Container widget level
              height: 400,
              child: Image.network(
                "https://i.imgur.com/LDG1v9c.jpg",
                fit: BoxFit.cover,
              ),
            ),
            Text(""),
            Container(
              margin: const EdgeInsets.only(top: 8, bottom: 8),
              child : Theme (
                data: ThemeData(
                    iconTheme: IconThemeData (
                      color: Colors.white,
                    )
                ),
                child: profileActionItems(),
              ),
            ),
            Divider(
              color: Colors.grey,
            ),
            TodayListTile(),
            TomorrowListTile(),

          ],
        )
      ]
  );
}
Widget profileActionItems() {
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        buildWeather1(),
        buildWeather2(),
        buildWeather3(),
        buildWeather4(),
        buildWeather5(),
        buildWeather6(),
      ]
  );
}
