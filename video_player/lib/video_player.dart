import 'package:flutter/material.dart';

class VideoPlayer extends StatelessWidget {
  const VideoPlayer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return width > height
        ? Stack(children: [
            Center(
              child: AspectRatio(
                aspectRatio: 16 / 9,
                child: Container(
                  color: Colors.black,
                  child: const Center(
                    child: Icon(
                      Icons.play_circle,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),

            // const VideoDescription(),
            Align(
              alignment: Alignment.topRight,
              child: IconButton(
                  onPressed: () {
                    showDialog(
                        context: context,
                        builder: (context) {
                          return AlertDialog(
                            content: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: const [VideoDescription()],
                            ),
                            actions: [
                              TextButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                  child: const Text(
                                    "Close",
                                    style: TextStyle(color: Colors.black),
                                  )),
                            ],
                          );
                        });
                  },
                  icon: const Icon(
                    Icons.info,
                    color: Colors.green,
                  )),
            )
          ])
        : Column(children: [
            AspectRatio(
              aspectRatio: 16 / 9,
              child: Container(
                color: Colors.black,
                child: const Center(
                  child: Icon(
                    Icons.play_circle,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            const VideoDescription(),
          ]);
  }
}

class VideoDescription extends StatelessWidget {
  const VideoDescription({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: const [
          Text(
            'Video Title',
            overflow: TextOverflow.ellipsis,
            style: TextStyle(fontSize: 24),
          ),
          Text(
            'Video description',
            overflow: TextOverflow.ellipsis,
            style: TextStyle(fontSize: 18),
          ),
          Text(
            'Likes: 34',
            overflow: TextOverflow.ellipsis,
            style: TextStyle(fontSize: 16),
          ),
        ],
      ),
    );
  }
}
