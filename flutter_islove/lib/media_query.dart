import 'package:flutter/material.dart';

class MediaQueryInfo extends StatelessWidget {
  final double size;

  const MediaQueryInfo({Key? key, required this.size}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;

    return screenWidth > 400
        ? Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              FlutterLogo(size: size),
              Icon(Icons.favorite, color: Colors.red, size: size),
            ],
          )
        : Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
            FlutterLogo(size: size),
            Icon(Icons.favorite, color: Colors.red, size: size),
          ]);
  }
}
