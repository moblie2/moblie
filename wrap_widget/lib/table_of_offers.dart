import 'package:flutter/material.dart';

import 'offer.dart';

class TableOfOffers extends StatelessWidget {
  const TableOfOffers({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Table(
      children: const [
        TableRow(
          children: [
            Offer(
              price: 19,
              description: '1 GB storage',
              title: 'Basic',
            ),
            Offer(
              price: 39,
              description: '5 GB storage',
              title: 'Family',
            ),
          ],
        ),
        TableRow(
          children: [
            Offer(
              price: 69,
              description: '10 GB storage',
              title: 'Pro',
            ),
            Offer(
              price: 89,
              description: 'Unlimited storage',
              title: 'Enterprise',
            ),
          ],
        ),
      ],
    );
  }
}
